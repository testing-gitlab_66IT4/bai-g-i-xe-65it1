-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2022 at 03:17 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_qlbx_clone`
--

-- --------------------------------------------------------

--
-- Table structure for table `chodexe`
--

CREATE TABLE `chodexe` (
  `ID` varchar(10) NOT NULL,
  `LoaiChoDe` varchar(10) NOT NULL,
  `SoLuongToiDa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `chodexe`
--

INSERT INTO `chodexe` (`ID`, `LoaiChoDe`, `SoLuongToiDa`) VALUES
('01', 'XeMay', 2000),
('02', 'Oto', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `dangkyvethang`
--

CREATE TABLE `dangkyvethang` (
  `ID` int(11) NOT NULL,
  `TenKhach` text NOT NULL,
  `SoDienThoai` varchar(10) NOT NULL,
  `ThoiGianDangKy` datetime NOT NULL,
  `ThoiGianKetThuc` datetime NOT NULL,
  `IDVeThang` varchar(10) NOT NULL,
  `BienSoXe` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nhansu`
--

CREATE TABLE `nhansu` (
  `SoDienThoai` varchar(10) NOT NULL,
  `Ten` text NOT NULL,
  `ChucVu` bit(1) NOT NULL,
  `MatKhau` varchar(30) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `nhansu`
--

INSERT INTO `nhansu` (`SoDienThoai`, `Ten`, `ChucVu`, `MatKhau`, `ID`) VALUES
('2', 'ADMIN', b'1', '2', 8);

-- --------------------------------------------------------

--
-- Table structure for table `suco`
--

CREATE TABLE `suco` (
  `ThoiGianGui` datetime NOT NULL,
  `Loai` text NOT NULL,
  `MoTa` text NOT NULL,
  `IsKhachDenBu` bit(1) NOT NULL,
  `Cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vengay`
--

CREATE TABLE `vengay` (
  `MaVe` varchar(10) NOT NULL,
  `GiaVeNgay` int(11) NOT NULL,
  `TrangThai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `vengay`
--

INSERT INTO `vengay` (`MaVe`, `GiaVeNgay`, `TrangThai`) VALUES
('VN0001', 3000, 0),
('VN0002', 3000, 0),
('VN0003', 3000, 0),
('VN0004', 3000, 0),
('VN0005', 3000, 0),
('VN0006', 3000, 0),
('VN0007', 3000, 0),
('VN0008', 3000, 0),
('VN0011', 3000, 0),
('VN0012', 3000, 0),
('VN0013', 3000, 0),
('VN0014', 3000, 0),
('VN0015', 3000, 0),
('VN0016', 3000, 0),
('VN0017', 3000, 0),
('VN0018', 3000, 0),
('VN0019', 3000, 0),
('VN0020', 3000, 0),
('VN0021', 3000, 0),
('VN0022', 3000, 0),
('VN0023', 3000, 0),
('VN0024', 3000, 0),
('VN0025', 3000, 0),
('VN0026', 3000, 0),
('VN0027', 3000, 0),
('VN0028', 3000, 0),
('VN0029', 3000, 0),
('VN0030', 3000, 0),
('VN0031', 3000, 0),
('VN0032', 3000, 0),
('VN0033', 3000, 0),
('VN0034', 3000, 0),
('VN0035', 3000, 0),
('VN0036', 3000, 0),
('VN0037', 3000, 0),
('VN0038', 3000, 0),
('VN0039', 3000, 0),
('VN0040', 3000, 0),
('VN0041', 3000, 0),
('VN0042', 3000, 0),
('VN0043', 3000, 0),
('VN0044', 3000, 0),
('VN0045', 3000, 0),
('VN0046', 3000, 0),
('VN0047', 3000, 0),
('VN0048', 3000, 0),
('VN0049', 3000, 0),
('VN0050', 3000, 0),
('VN0051', 3000, 0),
('VN0052', 3000, 0),
('VN0053', 3000, 0),
('VN0054', 3000, 0),
('VN0055', 3000, 0),
('VN0056', 3000, 0),
('VN0057', 3000, 0),
('VN0058', 3000, 0),
('VN0059', 3000, 0),
('VN0060', 3000, 0),
('VN0061', 3000, 0),
('VN0062', 3000, 0),
('VN0063', 3000, 0),
('VN0064', 3000, 0),
('VN0065', 3000, 0),
('VN0066', 3000, 0),
('VN0067', 3000, 0),
('VN0068', 3000, 0),
('VN0069', 3000, 0),
('VN0070', 3000, 0),
('VN0071', 3000, 0),
('VN0072', 3000, 0),
('VN0073', 3000, 0),
('VN0074', 3000, 0),
('VN0075', 3000, 0),
('VN0076', 3000, 0),
('VN0077', 3000, 0),
('VN0078', 3000, 0),
('VN0079', 3000, 0),
('VN0080', 3000, 0),
('VN0081', 3000, 0),
('VN0082', 3000, 0),
('VN0083', 3000, 0),
('VN0084', 3000, 0),
('VN0085', 3000, 0),
('VN0086', 3000, 0),
('VN0087', 3000, 0),
('VN0088', 3000, 0),
('VN0089', 3000, 0),
('VN0090', 3000, 0),
('VN0091', 3000, 0),
('VN0092', 3000, 0),
('VN0093', 3000, 0),
('VN0094', 3000, 0),
('VN0095', 3000, 0),
('VN0096', 3000, 0),
('VN0097', 3000, 0),
('VN0098', 3000, 0),
('VN0099', 3000, 0),
('VN1001', 20000, 0),
('VN1002', 20000, 0),
('VN1003', 20000, 0),
('VN1004', 20000, 0),
('VN1005', 20000, 0),
('VN1006', 20000, 0),
('VN1007', 20000, 0),
('VN1008', 20000, 0),
('VN1009', 20000, 0),
('VN1011', 20000, 0),
('VN1012', 20000, 0),
('VN1013', 20000, 0),
('VN1014', 20000, 0),
('VN1015', 20000, 0),
('VN1016', 20000, 0),
('VN1017', 20000, 0),
('VN1018', 20000, 0),
('VN1019', 20000, 0),
('VN1020', 20000, 0),
('VN1021', 20000, 0),
('VN1022', 20000, 0),
('VN1023', 20000, 0),
('VN1024', 20000, 0),
('VN1025', 20000, 0),
('VN1026', 20000, 0),
('VN1027', 20000, 0),
('VN1028', 20000, 0),
('VN1029', 20000, 0),
('VN1030', 20000, 0),
('VN1031', 20000, 0),
('VN1032', 20000, 0),
('VN1033', 20000, 0),
('VN1034', 20000, 0),
('VN1035', 20000, 0),
('VN1036', 20000, 0),
('VN1037', 20000, 0),
('VN1038', 20000, 0),
('VN1039', 20000, 0),
('VN1040', 20000, 0),
('VN1041', 20000, 0),
('VN1042', 20000, 0),
('VN1043', 20000, 0),
('VN1044', 20000, 0),
('VN1045', 20000, 0),
('VN1046', 20000, 0),
('VN1047', 20000, 0),
('VN1048', 20000, 0),
('VN1049', 20000, 0),
('VN1050', 20000, 0),
('VN1051', 20000, 0),
('VN1052', 20000, 0),
('VN1053', 20000, 0),
('VN1054', 20000, 0),
('VN1055', 20000, 0),
('VN1056', 20000, 0),
('VN1057', 20000, 0),
('VN1058', 20000, 0),
('VN1059', 20000, 0),
('VN1060', 20000, 0),
('VN1061', 20000, 0),
('VN1062', 20000, 0),
('VN1063', 20000, 0),
('VN1064', 20000, 0),
('VN1065', 20000, 0),
('VN1066', 20000, 0),
('VN1067', 20000, 0),
('VN1068', 20000, 0),
('VN1069', 20000, 0),
('VN1070', 20000, 0),
('VN1071', 20000, 0),
('VN1072', 20000, 0),
('VN1073', 20000, 0),
('VN1074', 20000, 0),
('VN1075', 20000, 0),
('VN1076', 20000, 0),
('VN1077', 20000, 0),
('VN1078', 20000, 0),
('VN1079', 20000, 0),
('VN1080', 20000, 0),
('VN1081', 20000, 0),
('VN1082', 20000, 0),
('VN1083', 20000, 0),
('VN1084', 20000, 0),
('VN1085', 20000, 0),
('VN1086', 20000, 0),
('VN1087', 20000, 0),
('VN1088', 20000, 0),
('VN1089', 20000, 0),
('VN1090', 20000, 0),
('VN1091', 20000, 0),
('VN1092', 20000, 0),
('VN1093', 20000, 0),
('VN1094', 20000, 0),
('VN1095', 20000, 0),
('VN1096', 20000, 0),
('VN1097', 20000, 0),
('VN1098', 20000, 0),
('VN1099', 20000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vengaygui`
--

CREATE TABLE `vengaygui` (
  `ThoiGianGui` datetime NOT NULL,
  `IDChoDe` varchar(10) NOT NULL,
  `IDVe` varchar(10) NOT NULL,
  `BienSoXe` varchar(10) NOT NULL,
  `ThoiGianTra` datetime DEFAULT NULL,
  `Gia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vethang`
--

CREATE TABLE `vethang` (
  `MaVe` varchar(10) NOT NULL,
  `GiaVeThang` int(11) NOT NULL,
  `TrangThai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `vethang`
--

INSERT INTO `vethang` (`MaVe`, `GiaVeThang`, `TrangThai`) VALUES
('VT0001', 80000, 0),
('VT0002', 80000, 0),
('VT0003', 80000, 0),
('VT0004', 80000, 0),
('VT0005', 80000, 0),
('VT0006', 80000, 0),
('VT0007', 80000, 0),
('VT0008', 80000, 0),
('VT0009', 80000, 0),
('VT0010', 80000, 0),
('VT0011', 80000, 0),
('VT0012', 80000, 0),
('VT0013', 80000, 0),
('VT0014', 80000, 0),
('VT0015', 80000, 0),
('VT0016', 80000, 0),
('VT0017', 80000, 0),
('VT0018', 80000, 0),
('VT0019', 80000, 0),
('VT0020', 80000, 0),
('VT0021', 80000, 0),
('VT0022', 80000, 0),
('VT0023', 80000, 0),
('VT0024', 80000, 0),
('VT0025', 80000, 0),
('VT0026', 80000, 0),
('VT0027', 80000, 0),
('VT0028', 80000, 0),
('VT0029', 80000, 0),
('VT0030', 80000, 0),
('VT0031', 80000, 0),
('VT0032', 80000, 0),
('VT0033', 80000, 0),
('VT0034', 80000, 0),
('VT0035', 80000, 0),
('VT0036', 80000, 0),
('VT0037', 80000, 0),
('VT0038', 80000, 0),
('VT0039', 80000, 0),
('VT0040', 80000, 0),
('VT0041', 80000, 0),
('VT0042', 80000, 0),
('VT0043', 80000, 0),
('VT0044', 80000, 0),
('VT0045', 80000, 0),
('VT0046', 80000, 0),
('VT0047', 80000, 0),
('VT0048', 80000, 0),
('VT0049', 80000, 0),
('VT0050', 80000, 0),
('VT0051', 80000, 0),
('VT0052', 80000, 0),
('VT0053', 80000, 0),
('VT0054', 80000, 0),
('VT0055', 80000, 0),
('VT0056', 80000, 0),
('VT0057', 80000, 0),
('VT0058', 80000, 0),
('VT0059', 80000, 0),
('VT0060', 80000, 0),
('VT0061', 80000, 0),
('VT0062', 80000, 0),
('VT0063', 80000, 0),
('VT0064', 80000, 0),
('VT0065', 80000, 0),
('VT0066', 80000, 0),
('VT0067', 80000, 0),
('VT0068', 80000, 0),
('VT0069', 80000, 0),
('VT0070', 80000, 0),
('VT0071', 80000, 0),
('VT0072', 80000, 0),
('VT0073', 80000, 0),
('VT0074', 80000, 0),
('VT0075', 80000, 0),
('VT0076', 80000, 0),
('VT0077', 80000, 0),
('VT0078', 80000, 0),
('VT0079', 80000, 0),
('VT0080', 80000, 0),
('VT0081', 80000, 0),
('VT0082', 80000, 0),
('VT0083', 80000, 0),
('VT0084', 80000, 0),
('VT0085', 80000, 0),
('VT0086', 80000, 0),
('VT0087', 80000, 0),
('VT0088', 80000, 0),
('VT0089', 80000, 0),
('VT0090', 80000, 0),
('VT0091', 80000, 0),
('VT0092', 80000, 0),
('VT0093', 80000, 0),
('VT0094', 80000, 0),
('VT0095', 80000, 0),
('VT0096', 80000, 0),
('VT0097', 80000, 0),
('VT0098', 80000, 0),
('VT0099', 80000, 0),
('VT1001', 580000, 0),
('VT1002', 580000, 0),
('VT1003', 580000, 0),
('VT1004', 580000, 0),
('VT1005', 580000, 0),
('VT1006', 580000, 0),
('VT1007', 580000, 0),
('VT1008', 580000, 0),
('VT1009', 580000, 0),
('VT1010', 580000, 0),
('VT1011', 580000, 0),
('VT1012', 580000, 0),
('VT1013', 580000, 0),
('VT1014', 580000, 0),
('VT1015', 580000, 0),
('VT1016', 580000, 0),
('VT1017', 580000, 0),
('VT1018', 580000, 0),
('VT1019', 580000, 0),
('VT1020', 580000, 0),
('VT1021', 580000, 0),
('VT1022', 580000, 0),
('VT1023', 580000, 0),
('VT1024', 580000, 0),
('VT1025', 580000, 0),
('VT1026', 580000, 0),
('VT1027', 580000, 0),
('VT1028', 580000, 0),
('VT1029', 580000, 0),
('VT1030', 580000, 0),
('VT1031', 580000, 0),
('VT1032', 580000, 0),
('VT1033', 580000, 0),
('VT1034', 580000, 0),
('VT1035', 580000, 0),
('VT1036', 580000, 0),
('VT1037', 580000, 0),
('VT1038', 580000, 0),
('VT1039', 580000, 0),
('VT1040', 580000, 0),
('VT1041', 580000, 0),
('VT1042', 580000, 0),
('VT1043', 580000, 0),
('VT1044', 580000, 0),
('VT1045', 580000, 0),
('VT1046', 580000, 0),
('VT1047', 580000, 0),
('VT1048', 580000, 0),
('VT1049', 580000, 0),
('VT1050', 580000, 0),
('VT1051', 580000, 0),
('VT1052', 580000, 0),
('VT1053', 580000, 0),
('VT1054', 580000, 0),
('VT1055', 580000, 0),
('VT1056', 580000, 0),
('VT1057', 580000, 0),
('VT1058', 580000, 0),
('VT1059', 580000, 0),
('VT1060', 580000, 0),
('VT1061', 580000, 0),
('VT1062', 580000, 0),
('VT1063', 580000, 0),
('VT1064', 580000, 0),
('VT1065', 580000, 0),
('VT1066', 580000, 0),
('VT1067', 580000, 0),
('VT1068', 580000, 0),
('VT1069', 580000, 0),
('VT1070', 580000, 0),
('VT1071', 580000, 0),
('VT1072', 580000, 0),
('VT1073', 580000, 0),
('VT1074', 580000, 0),
('VT1075', 580000, 0),
('VT1076', 580000, 0),
('VT1077', 580000, 0),
('VT1078', 580000, 0),
('VT1079', 580000, 0),
('VT1080', 580000, 0),
('VT1081', 580000, 0),
('VT1082', 580000, 0),
('VT1083', 580000, 0),
('VT1084', 580000, 0),
('VT1085', 580000, 0),
('VT1086', 580000, 0),
('VT1087', 580000, 0),
('VT1088', 580000, 0),
('VT1089', 580000, 0),
('VT1090', 580000, 0),
('VT1091', 580000, 0),
('VT1092', 580000, 0),
('VT1093', 580000, 0),
('VT1094', 580000, 0),
('VT1095', 580000, 0),
('VT1096', 580000, 0),
('VT1097', 580000, 0),
('VT1098', 580000, 0),
('VT1099', 580000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vethanggui`
--

CREATE TABLE `vethanggui` (
  `ThoiGianGui` datetime NOT NULL,
  `IDChoDe` varchar(10) NOT NULL,
  `IDVe` varchar(10) NOT NULL,
  `BienSoXe` varchar(10) NOT NULL,
  `ThoiGianTra` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chodexe`
--
ALTER TABLE `chodexe`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `dangkyvethang`
--
ALTER TABLE `dangkyvethang`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KhoaNgoai_IDVeThang` (`IDVeThang`);

--
-- Indexes for table `nhansu`
--
ALTER TABLE `nhansu`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `suco`
--
ALTER TABLE `suco`
  ADD PRIMARY KEY (`ThoiGianGui`);

--
-- Indexes for table `vengay`
--
ALTER TABLE `vengay`
  ADD PRIMARY KEY (`MaVe`);

--
-- Indexes for table `vengaygui`
--
ALTER TABLE `vengaygui`
  ADD PRIMARY KEY (`ThoiGianGui`,`IDChoDe`,`IDVe`),
  ADD KEY `KhoaNgoai_IDChoDeXe` (`IDChoDe`),
  ADD KEY `KhoaNgoai_IDVe` (`IDVe`);

--
-- Indexes for table `vethang`
--
ALTER TABLE `vethang`
  ADD PRIMARY KEY (`MaVe`);

--
-- Indexes for table `vethanggui`
--
ALTER TABLE `vethanggui`
  ADD PRIMARY KEY (`ThoiGianGui`,`IDChoDe`,`IDVe`),
  ADD KEY `KhoaNgoai_IDChoDeXeThang` (`IDChoDe`),
  ADD KEY `KhoaNgoai_IDGuiVeThang` (`IDVe`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dangkyvethang`
--
ALTER TABLE `dangkyvethang`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `nhansu`
--
ALTER TABLE `nhansu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dangkyvethang`
--
ALTER TABLE `dangkyvethang`
  ADD CONSTRAINT `KhoaNgoai_IDVeThang` FOREIGN KEY (`IDVeThang`) REFERENCES `vethang` (`MaVe`);

--
-- Constraints for table `vengaygui`
--
ALTER TABLE `vengaygui`
  ADD CONSTRAINT `KhoaNgoai_IDChoDeXe` FOREIGN KEY (`IDChoDe`) REFERENCES `chodexe` (`ID`),
  ADD CONSTRAINT `KhoaNgoai_IDVe` FOREIGN KEY (`IDVe`) REFERENCES `vengay` (`MaVe`);

--
-- Constraints for table `vethanggui`
--
ALTER TABLE `vethanggui`
  ADD CONSTRAINT `KhoaNgoai_IDChoDeXeThang` FOREIGN KEY (`IDChoDe`) REFERENCES `chodexe` (`ID`),
  ADD CONSTRAINT `KhoaNgoai_IDGuiVeThang` FOREIGN KEY (`IDVe`) REFERENCES `vethang` (`MaVe`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
